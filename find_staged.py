#!/usr/bin/env python
import os
from itertools import izip_longest
from subprocess import Popen, PIPE, STDOUT
from os.path import join, dirname, basename, isdir, isfile

import XRootD.client


def grouper(iterable, n):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3) --> ABC DEF G
    args = [iter(iterable)] * n
    return map(lambda x: filter(None, x), izip_longest(fillvalue=None, *args))


def list_dir(path):
    status, result = castor.dirlist(path)
    assert status.ok, status
    return [d.name for d in result.dirlist]


castor = XRootD.client.FileSystem('root://castorlhcb.cern.ch/')

base_dir = '/castor/cern.ch/grid/lhcb/data/2017/RAW/'
data_types = list_dir(base_dir)
for data_type in data_types:
    if data_type in ['FULL', 'TURBO', 'TURBORAW', 'SMOGPHY', 'TEST_MARKUS_CALIB']:
        continue
    collision_dir = join(base_dir, data_type, 'LHCb', 'COLLISION17')
    runs = list_dir(collision_dir)
    files = []
    for run in runs:
        files.extend([join(collision_dir, run, fn)
                      for fn in list_dir(join(collision_dir, run))])

    # Check which files are still staged
    staged = []
    not_staged = []
    for group in grouper(files, 1000):
        process = Popen('stager_qry -M '+' -M '.join(group), stderr=STDOUT,
                        stdout=PIPE, shell=True)
        output = process.communicate()[0].split('\n')

        for fn in group:
            matches = [s for s in output if fn in s]
            assert len(matches) == 1, matches
            if 'not on disk cache' in matches[0]:
                not_staged.append(fn)
            elif ' STAGED' in matches[0]:
                staged.append(fn)
            elif any([' '+s in matches[0] for s in ['STAGEIN', 'CANBEMIGR', 'STAGEOUT']]):
                # These files probably won't be staged when we need them
                not_staged.append(fn)
            else:
                raise RuntimeError(matches)

    print('Found', len(staged), 'files for', collision_dir, 'out of',
          len(staged)+len(not_staged))

    for fn in staged:
        run = basename(dirname(fn))
        output_dirname = join('output', data_type, run)
        if not isdir(output_dirname):
            os.makedirs(output_dirname)
        output_filename = join(output_dirname, basename(fn))
        if not isfile(output_filename):
            with open(output_filename, 'wt'):
                pass
