
## Running

```bash
lb-run --ext=xrootd_python Urania/latest ./find_staged.py
lb-run Bender/latest bender root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2017/RAW/NOBIAS/LHCb/COLLISION17/195508/195508_0000000096.raw
```

https://lbrundb.cern.ch/api/fill/6097/
{
  "avHltPhysRate": "74992.858569937",
  "avL0PhysRate": "659641.34764398",
  "avLumi": "310.15530460423",
  "avMu": "1.0862037643263",
  "avPhysDeadTime": "5.3950046181914",
  "crossingAngle": "-210",
  "fill_id": 6097,
  "lumi_hvon": 13207934.18,
  "lumi_logged": 12378864.880000001,
  "lumi_running": 13009952.880000001,
  "lumi_total": 13310763.85,
  "lumi_veloin": 13189200.449999999,
  "nBunchesB1": "1740",
  "nBunchesB2": "1740",
  "nCollidingBunches": "1636",
  "peakHltPhysRate": "79682.7921875",
  "peakL0PhysRate": "706650.85069508",
  "peakLumi": "334.29128003014",
  "peakMu": "1.1707310542812",
  "peakPhysDeadTime": "100",
  "prev_fill_id": 6096,
  "start_date": "2017-08-17T23:42:31",
  "time_hvon": 42530.212,
  "time_logged": 39741.031199999998,
  "time_running": 41763.438000000002,
  "time_total": 42919.266000000003,
  "time_veloin": 42338.061999999998,
  "timestamp": "2017-08-18T11:37:51",
  "veloLostLumi": "18135.496831968",
  "veloLostTime": "240"
}

https://lbrundb.cern.ch/api/run/197451/
{
  "LHCState": "NO_BEAM",
  "activity": "EOF_CALIB",
  "avHltPhysRate": "0",
  "avL0PhysRate": "0",
  "avLumi": "9.6416837946199e-05",
  "avMu": "0",
  "avPhysDeadTime": "3.7915265583925",
  "beamenergy": 6500.0,
  "beamgasTrigger": "0",
  "betaStar": "9.87",
  "conddbTag": "cond-20170510",
  "dddbTag": "dddb-20150724",
  "destination": "LOCAL",
  "endlumi": -12378864.877803,
  "endtime": "2017-08-18T12:50:06",
  "fillid": 6097,
  "lumiTrigger": "1",
  "magnetCurrent": "5850",
  "magnetState": "UP",
  "next_runid": 197452,
  "nobiasTrigger": "0",
  "onldbTag": "onl-20170512",
  "partitionid": 65535,
  "partitionname": "LHCb",
  "prev_runid": 197449,
  "program": "DEFAULT",
  "programVersion": "UNKNOWN",
  "run_state": 2,
  "runid": 197451,
  "runtype": "EOF_CALIB17",
  "startlumi": 0.0,
  "starttime": "2017-08-18T12:23:15",
  "state": "ENDED",
  "tck": "-2137456632",
  "triggerConfiguration": "EofCalib",
  "veloOpening": "57.998498916626",
  "veloPosition": "Open"
}