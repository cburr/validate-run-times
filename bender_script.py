import subprocess
import tempfile
from os.path import join, isfile, basename
import os
import json
from glob import glob


if False:
    evtSel = None
    evt = None
    run = None


base_dir = '/castor/cern.ch/grid/lhcb/data/2017/RAW/'
server = 'root://castorlhcb.cern.ch/'

temp_dir = tempfile.mkdtemp()
for data_type in ['CALIB']:  # os.listdir('output'):
    for i, run_number in enumerate(os.listdir(join('output', data_type))):
        for fn in glob(join('output', data_type, run_number, '*.raw')):
            fn = basename(fn)
            json_fn = join('output', data_type, run_number, fn+'.json')
            if isfile(json_fn):
                continue
            print('Running for', join('output', data_type, run_number, fn))
            source_fn = server + join(base_dir, data_type, 'LHCb', 'COLLISION17', run_number, fn)
            temp_fn = join(temp_dir, fn)
            subprocess.check_call('xrdcp ' + source_fn + ' ' + temp_fn, shell=True)
            evtSel.open(temp_fn)
            odin_info = []
            i = 0
            while run(1).isSuccess():
                i += 1
                odin = evt['/Event/DAQ/ODIN']
                odin_info.append([
                    i, odin.gpsTime(), odin.runNumber(), odin.orbitNumber(),
                    odin.eventNumber(), odin.detectorStatus()
                ])
            with open(json_fn, 'wt') as fp:
                json.dump(odin_info, fp)
            os.remove(join(temp_dir, fn))
